worker_processes  2;
pid        /tmp/nginx.pid;
include /etc/nginx/modules-enabled/*.conf;
load_module /etc/nginx/modules/ngx_http_headers_more_filter_module.so;
load_module /etc/nginx/modules/ngx_http_geoip2_module.so;
events {
    worker_connections  4096;
    # Experimental https://docs.bluehosting.cl/tutoriales/servidores/como-configurar-nginx-para-obtener-un-rendimiento-optimo.html
    use epoll;
    multi_accept on;
}


http {
    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;


    geoip2 /etc/GeoLite2-Country.mmdb {
        auto_reload 5m;
        $geoip2_metadata_country_build metadata build_epoch;
        $geoip2_data_country_name country names en;
        $geoip2_data_country_code country iso_code;
    }

    # Don't show server header 
    server_tokens off;
    more_clear_headers Server;
    more_clear_headers 'x-generator';    
    client_body_in_file_only off;
    log_format  main  '$remote_addr - $geoip2_data_country_code - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    log_format json_analytics escape=json '{'
                        '"msec": "$msec", ' # request unixtime in seconds with a milliseconds resolution
                        '"connection": "$connection", ' # connection serial number
                        '"connection_requests": "$connection_requests", ' # number of requests made in connection
                        '"pid": "$pid", ' # process pid
                        '"request_id": "$request_id", ' # the unique request id
                        '"request_length": "$request_length", ' # request length (including headers and body)
                        '"remote_addr": "$remote_addr", ' # client IP
                        '"remote_user": "$remote_user", ' # client HTTP username
                        '"remote_port": "$remote_port", ' # client port
                        '"time_local": "$time_local", '
                        '"time_iso8601": "$time_iso8601", ' # local time in the ISO 8601 standard format
                        '"request": "$request", ' # full path no arguments if the request
                        '"request_uri": "$request_uri", ' # full path and arguments if the request
                        '"args": "$args", ' # args
                        '"status": "$status", ' # response status code
                        '"body_bytes_sent": "$body_bytes_sent", ' # the number of body bytes exclude headers sent to a client
                        '"bytes_sent": "$bytes_sent", ' # the number of bytes sent to a client
                        '"http_referer": "$http_referer", ' # HTTP referer
                        '"http_user_agent": "$http_user_agent", ' # user agent
                        '"http_x_forwarded_for": "$http_x_forwarded_for", ' # http_x_forwarded_for
                        '"http_host": "$http_host", ' # the request Host: header
                        '"server_name": "$server_name", ' # the name of the vhost serving the request
                        '"request_time": "$request_time", ' # request processing time in seconds with msec resolution
                        '"upstream": "$upstream_addr", ' # upstream backend server for proxied requests
                        '"upstream_connect_time": "$upstream_connect_time", ' # upstream handshake time incl. TLS
                        '"upstream_header_time": "$upstream_header_time", ' # time spent receiving upstream headers
                        '"upstream_response_time": "$upstream_response_time", ' # time spend receiving upstream body
                        '"upstream_response_length": "$upstream_response_length", ' # upstream response length
                        '"upstream_cache_status": "$upstream_cache_status", ' # cache HIT/MISS where applicable
                        '"ssl_protocol": "$ssl_protocol", ' # TLS protocol
                        '"ssl_cipher": "$ssl_cipher", ' # TLS cipher
                        '"scheme": "$scheme", ' # http or https
                        '"request_method": "$request_method", ' # request method
                        '"server_protocol": "$server_protocol", ' # request protocol, like HTTP/1.1 or HTTP/2.0
                        '"pipe": "$pipe", ' # "p" if request was pipelined, "." otherwise
                        '"gzip_ratio": "$gzip_ratio", '
                        '"http_cf_ray": "$http_cf_ray",'
                        '"geoip_country_code": "$geoip2_data_country_code"'
                        '}';

    # https://thoughts.t37.net/nginx-optimization-understanding-sendfile-tcp-nodelay-and-tcp-nopush-c55cdd276765
    sendfile        on;
    tcp_nopush     on;
    tcp_nodelay on;

    keepalive_timeout  65;

    include /etc/nginx/conf.d/*.conf;
}
