FROM nginx:1.19.6

ENV MAXMIND_VERSION="1.6.0"

# auto-letsencrypt configuration
RUN mkdir -p /tmp/letsencrypt/www && chown -R www-data:www-data /tmp/letsencrypt/www
VOLUME [ "/tmp/letsencrypt/www/" ]

# module configuration
RUN apt update -y && apt-get -y install curl build-essential libpcre3 libpcre3-dev zlib1g-dev libssl-dev git vim

WORKDIR /tmp
# Add dynamic module

# VTS
RUN curl -LO http://nginx.org/download/nginx-1.19.6.tar.gz && \
    tar zxf nginx-1.19.6.tar.gz && \
    cd nginx-1.19.6 && \
    git clone -b master https://github.com/vozlt/nginx-module-vts.git && \
    ./configure --with-http_ssl_module --with-compat --add-dynamic-module=nginx-module-vts && \
    make modules && \
    cp /tmp/nginx-1.19.6/objs/ngx_http_vhost_traffic_status_module.so  /etc/nginx/modules/ngx_http_vhost_traffic_status_module.so  && \
    cd /tmp && \
    rm -f nginx-1.19.6.tar.gz && \
    rm -rf nginx-1.19.6

# MORE HEADERS
RUN curl -LO http://nginx.org/download/nginx-1.19.6.tar.gz && \
    tar zxf nginx-1.19.6.tar.gz && \
    cd nginx-1.19.6 && \
    git clone -b master https://github.com/openresty/headers-more-nginx-module.git  && \
    ./configure --with-compat --add-dynamic-module=headers-more-nginx-module && \
    make modules && \
    cp /tmp/nginx-1.19.6/objs/ngx_http_headers_more_filter_module.so  /etc/nginx/modules/ngx_http_headers_more_filter_module.so  && \
    cd /tmp && \
    rm -f nginx-1.19.6.tar.gz && \
    rm -rf nginx-1.19.6 && \
    apt-get autoremove -y

# AWS
RUN curl -LO http://nginx.org/download/nginx-1.19.6.tar.gz && \
    tar zxf nginx-1.19.6.tar.gz && \
    cd nginx-1.19.6 && \
    git clone -b master https://github.com/kaltura/nginx-aws-auth-module.git && \
    ./configure --with-http_ssl_module --with-compat --add-dynamic-module=nginx-aws-auth-module && \
    make modules && \
    cp /tmp/nginx-1.19.6/objs/ngx_http_aws_auth_module.so /etc/nginx/modules/ngx_http_aws_auth_module.so && \
    cd /tmp && \
    rm -f nginx-1.19.6.tar.gz && \
    rm -rf nginx-1.19.6

# GEO IP DB
COPY ./GeoLite2-Country.mmdb /etc/GeoLite2-Country.mmdb

RUN curl -LO https://github.com/maxmind/libmaxminddb/releases/download/${MAXMIND_VERSION}/libmaxminddb-${MAXMIND_VERSION}.tar.gz && \
    tar -xzvf libmaxminddb-${MAXMIND_VERSION}.tar.gz && cd libmaxminddb-${MAXMIND_VERSION} && ./configure && \
    make check && make install && ldconfig && cd /tmp/ && rm -fr libmaxminddb-${MAXMIND_VERSION} libmaxminddb-${MAXMIND_VERSION}.tar.gz

# GEO IP MODULE
RUN curl -LO http://nginx.org/download/nginx-1.19.6.tar.gz && \
    tar zxf nginx-1.19.6.tar.gz && \
    cd nginx-1.19.6 && \
    git clone -b master https://github.com/leev/ngx_http_geoip2_module.git && \
    ./configure --with-http_ssl_module --with-compat --add-dynamic-module=ngx_http_geoip2_module && \
    make modules && \
    cp /tmp/nginx-1.19.6/objs/ngx_http_geoip2_module.so /etc/nginx/modules/ngx_http_geoip2_module.so && \
    cd /tmp && \
    rm -f nginx-1.19.6.tar.gz && \
    rm -rf nginx-1.19.6

COPY nginx.conf /etc/nginx/nginx.conf