# docker-nginx

This is intended to be used with: <https://gitlab.com/mrmilu/public-docker-images/auto-letsencrypt>

This repo will have the latest release tested with modules included (1.19.6 at the moment), it will also be compatible with auto-letsencrypt, and will include the next modules:

- AWS auth <https://github.com/kaltura/nginx-aws-auth-module> - auth on s3 for private files
- VTS <https://github.com/vozlt/nginx-module-vts> - monitoring
- MORE HEADERS <https://github.com/openresty/headers-more-nginx-module.git> - allows extra useful headers, for example removing every server info
- GEOIP2 <https://github.com/leev/ngx_http_geoip2_module> - allows mapping every request IP with a location.

Refer to the documentation of any of the modules for further configuration examples and usage. For safety and to avoid non-used modules, they will not be include in the default configuration, however you can add them like this if required at nginx.conf:

```ini
# vts
load_module /etc/nginx/modules/ngx_http_vhost_traffic_status_module.so;
# aws
load_module /etc/nginx/modules/ngx_http_aws_auth_module.so;
```

The only exception are more_headers and geoIP which are configured by default. There's also a "json_analytics" log format which can be used with promtail